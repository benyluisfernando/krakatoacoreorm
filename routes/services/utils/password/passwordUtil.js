const pool = require("../../../../connection/db");
const sysParam = require("../user_management/getSysParam");
const passEnc = require("./encryptionPass");

let loginParam, sessionParam;
const getParam = async () => {
  loginParam = await sysParam.getUMParam();
  //sessionParam = await sysParam.getSessionParam();
};

const insertPasswordHistory = async (userId, password) => {
  // await pool.query(
  //   "insert into pass_hist (user_id,pwd) values ($1,crypt($2,gen_salt('bf',4)))",
  //   [userId, password]
  // );
  let pass = await passEnc.encryptPass(password);
  await pool("pass_hist")
    .returning(["id", "user_id"])
    .insert({ user_id: userId, pwd: pass });
};

const updateChangePasswordTime = async (id) => {
  // await pool.query(
  //   "Update  public.cdid_tenant_user set last_change_password = to_timestamp($1 / 1000.0) where id = $2",
  //   [Date.now(), id]
  // );
  var now = new Date();
  now.setHours(now.getHours() + 7);
  await pool("cdid_tenant_user").returning(["*"]).where("id", id).update({
    last_change_password: pool.fn.now(),
  });
};

module.exports = {
  updateChangePasswordTime,
  insertPasswordHistory,
};
