const pool = require("../../../connection/db");

const checkIdle = async (data, config) => {
  try {
    let dateLastReq = new Date();
    let expireDate = new Date(data.expire_date);

    expireDate = expireDate.setTime(expireDate.getTime() + 7);
    console.log(expireDate.toUTCString);
    if (dateLastReq >= expireDate) {
      await pool("trx_sessionlog")
        .returning(["*"])
        .where("id", data.id)
        .update({
          status: 2,
        });
      return false;
    } else {
      let newSession = new Date();
      let newExpire = new Date(
        newSession.getTime() + config.SESSION_IDLE * 60000
      );

      const updateSession = await pool("trx_sessionlog")
        .returning(["id"])
        .where("id", data.id)
        .update({
          updated_at: newSession,
          expire_date: newExpire,
        });
      return updateSession;
    }
  } catch (err) {
    console.log(err);
    return;
  }
};

module.exports = {
  checkIdle,
};
