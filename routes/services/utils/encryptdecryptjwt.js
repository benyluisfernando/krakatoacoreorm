const jwt = require('jsonwebtoken');

function encryptdata(dataobject) {
    
    return jwt.sign(dataobject,'jika surga dan neraka tak pernah ada, masihkah kau bersujud kepadanya');
}

function decryptdata(dataobject) {
    
    return jwt.verify(dataobject, 'jika surga dan neraka tak pernah ada, masihkah kau bersujud kepadanya');
}


module.exports = {
    encryptdata,
    decryptdata
};