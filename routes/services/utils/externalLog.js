var logger = {
  logging: function (filename) {
    var log4js = require("log4js");

    log4js.configure({
      appenders: {
        http: {
          type: "multiFile",
          base: "log/",
          property: "categoryName",
          pattern: "-yyyy-MM-dd",
          extension: ".log",
        },
      },
      categories: {
        default: { appenders: ["http"], level: "debug" },
      },
    });

    var _log = log4js.getLogger(filename);
    return _log;
  },
};

module.exports = logger;
