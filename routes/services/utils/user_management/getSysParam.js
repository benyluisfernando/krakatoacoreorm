// const { Op } = require("sequelize");
const pool = require("../../../../connection/db");
// const sysparam = require("../../../../models/system_param");
const getUMParam = async () => {
  // let param = await pool.query(
  //   "SELECT * FROM public.system_param where category = $1",
  //   ["USER_MANAGEMENT"]
  // );
  // let param = await sysparam.findAll({
  //   where: {
  //     category: {
  //       [Op.eq]: "USER_MANAGEMENT"
  //     }
  //   }
  // });
  let param = await pool('system_param').where({
    category: 'USER_MANAGEMENT'
  }).select('*');
  let rowparam = param;
  // console.log(rowparam.length);
  let obj = {};
  rowparam.forEach(function (column) {
    var columnName = column.param_name;
    obj[columnName] = column.param_value;
  });
  //console.log(obj);

  return obj;
};

const getNEParam = async () => {
  // let param = await pool.query(
  //   "SELECT * FROM public.system_param where category = $1",
  //   ["NOTIFIKASI_EMAIL"]
  // );

  let param = await pool('system_param').where({
    category: 'NOTIFIKASI_EMAIL'
  }).select('*');



  let rowparam = param;
  let obj = {};
  rowparam.forEach(function (column) {
    var columnName = column.param_name;
    obj[columnName] = column.param_value;
  });
  // console.log(obj);
  return obj;
};

module.exports = { getUMParam, getNEParam };
