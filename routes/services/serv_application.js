const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
router.get("/appsbytenant", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;

    // console.log(">>>> GET TENANTS >>>> " + dcodeInfo.leveltenant);
    /*
    var query =
      "SELECT clis.id, clis.licensetokens, clis.licensejson, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, 
      cdid_tenant, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description 
      FROM public.cdid_liscenses clis 
      INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id 
      where clis.cdid_tenant=$1 and clis.active=1 and apptype > 0 
      order by clis.defaultactive desc;";

    const resp = await pool.query(query, [dcodeInfo.idtenant]);
    */
    const resp = await pool("cdid_liscenses as clis")
      .innerJoin("mst_application as mapp", "clis.id_application", "mapp.id")
      .where("clis.cdid_tenant", dcodeInfo.idtenant)
      .andWhere("clis.active", 1)
      .andWhere("mapp.apptype", ">", 0)
      .orderBy("clis.defaultactive", "desc")
      .select(
        "clis.id",
        "clis.licensetokens",
        "clis.licensejson",
        "clis.expiredate",
        "cdid_tenant",
        "clis.id_application",
        "clis.paidstatus",
        "clis.active",
        "clis.defaultactive",
        "mapp.appname",
        "mapp.applabel",
        "mapp.description"
      );

    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/retriveAppByTenantAndOrgId/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;
    const orgid = req.params.id;
    /*
    var query =
      "SELECT id, idorg, idapp, idtenant, idsubtenant FROM public.cdid_orgapplications where idorg =$1 and idtenant =$2";
    const resp = await pool.query(query, [orgid, dcodeInfo.idtenant]);
    */
    const resp = await pool("cdid_orgapplications")
      .where({
        idorg: orgid,
        idtenant: dcodeInfo.idtenant,
      })
      .select("id", "idorg", "idapp", "idtenant", "idsubtenant");

    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error retrive selected apps" });
  }
});
module.exports = router;
// router.get("/retriveAppByTenantAndOrgId/:id", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       var tipeModule = 1;
//       const orgid = req.params.id;
//       var query =
//         "SELECT id, idorg, idapp, idtenant, idsubtenant FROM public.cdid_orgapplications where idorg =$1 and idtenant = $2";
//       const resp = await pool.query(query, [orgid, dcodeInfo.idtenant]);

//       var jsonbody = resp.rows;

//       res.status(200).json({ status: 200, data: jsonbody });
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     res.status(500).json({ status: 500, data: "Error retrive selected apps" });
//   }
// });
