const detail = require("../models/configPassword.json");
const nodemailer = require("nodemailer");
const sysParam = require("./utils/user_management/getSysParam");
const hbs = require("nodemailer-express-handlebars");
const path = require("path");
let pathNotif = "./routes/emailnotif/";
let neParam;

const getParam = async () => {
  neParam = await sysParam.getNEParam();
};

var notifikasi = {
  sendMail: async function (token, user, callback) {
    try {
      await getParam();

      console.log("MASUK SEND MAIL >>>>");

      // create reusable transporter object using the default SMTP transport
      var secure;
      if (neParam.EMAIL_SECURE === "false") secure = false;
      else secure = true;

      console.log(secure);
      let transporter = nodemailer.createTransport({
        host: neParam.EMAIL_HOST,
        port: neParam.EMAIL_PORT,
        secure: secure, // true for 465, false for other ports
        auth: {
          user: neParam.EMAIL_USER,
          pass: neParam.EMAIL_PASS,
        },
      });

      const handlebarOptions = {
        viewEngine: {
          partialsDir: path.resolve(pathNotif),
          defaultLayout: false,
        },
        viewPath: path.resolve(pathNotif),
      };

      transporter.use("compile", hbs(handlebarOptions));

      let mailOptions = {
        from: neParam.EMAIL_FROM + "<example.gmail.com>", // sender address
        to: user.email, // list of receivers
        subject: neParam.EMAIL_SUBJECT, // Subject line
        template: "forgot",
        context: {
          http: neParam.EMAIL_HTTP, // replace {{neParam.EMAIL_HTTP}} with Adebola
          token: token, // replace {{token}} with My Company
        },
      };
      // send mail with defined transport object
      let info = await transporter.sendMail(mailOptions);

      callback(info);
      getResponse = {
        status: "00",
        notifCallback: info,
      };
      return getResponse;
    } catch (err) {
      console.log(err);
      return err;
    }
  },
  sendMailVerifikasi: async function (token, user, callback) {
    try {
      await getParam();

      console.log("MASUK SEND MAIL VERIFIKASI >>>>");

      // create reusable transporter object using the default SMTP transport
      var secure;
      if (neParam.EMAIL_SECURE === "false") secure = false;
      else secure = true;

      console.log(secure);
      let transporter = nodemailer.createTransport({
        host: neParam.EMAIL_HOST,
        port: neParam.EMAIL_PORT,
        secure: secure, // true for 465, false for other ports
        auth: {
          user: neParam.EMAIL_USER,
          pass: neParam.EMAIL_PASS,
        },
      });

      const handlebarOptions = {
        viewEngine: {
          partialsDir: path.resolve(pathNotif),
          defaultLayout: false,
        },
        viewPath: path.resolve(pathNotif),
      };

      transporter.use("compile", hbs(handlebarOptions));

      let mailOptions = {
        from: neParam.EMAIL_FROM + "<example.gmail.com>", // sender address
        to: user.email, // list of receivers
        subject: "Email Verification", // Subject line
        template: "verifikasi",
        context: {
          http: neParam.EMAIL_HTTP, //neParam.EMAIL_HTTP,
          token: token,
        },
      };
      // send mail with defined transport object
      let info = await transporter.sendMail(mailOptions);

      callback(info);
      getResponse = {
        status: "00",
        notifCallback: info,
      };
      return getResponse;
    } catch (err) {
      console.log(err);
      return err;
    }
  },
  sendMailNotifikasi: async function (user, callback) {
    try {
      await getParam();

      for (let emailRow of user) {
        if (emailRow.selected) {
          this.selectAllChecked = false;
          break;
        }
        console.log("MASUK SEND MAIL NOTFIKASI >>>>");
        console.log(emailRow);

        // create reusable transporter object using the default SMTP transport
        var secure;
        if (neParam.EMAIL_SECURE === "false") secure = false;
        else secure = true;

        let transporter = nodemailer.createTransport({
          host: neParam.EMAIL_HOST,
          port: neParam.EMAIL_PORT,
          secure: secure, // true for 465, false for other ports
          auth: {
            user: neParam.EMAIL_USER,
            pass: neParam.EMAIL_PASS,
          },
        });

        const handlebarOptions = {
          viewEngine: {
            partialsDir: path.resolve(pathNotif),
            defaultLayout: false,
          },
          viewPath: path.resolve(pathNotif),
        };

        transporter.use("compile", hbs(handlebarOptions));

        let mailOptions = {
          from: neParam.EMAIL_FROM + "<example.gmail.com>", // sender address
          to: emailRow, // list of receivers
          subject: "Email Notifikasi", // Subject line
          template: "notifikasi",
          context: {
            http: neParam.EMAIL_HTTP, //neParam.EMAIL_HTTP,
            name: emailRow,
          },
        };
        // send mail with defined transport object
        let info = await transporter.sendMail(mailOptions);

        callback(info);
        getResponse = {
          status: "00",
          notifCallback: info,
        };
      }
      return getResponse;
    } catch (err) {
      console.log(err);
      return err;
    }
  },
  sendMailMessage: async function (user, message, callback) {
    try {
      await getParam();
      console.log("message::::", message);

      for (let emailRow of user) {
        if (emailRow.selected) {
          this.selectAllChecked = false;
          break;
        }
        console.log("MASUK SEND MAIL NOTFIKASI >>>>");
        console.log(emailRow);

        // create reusable transporter object using the default SMTP transport
        var secure;
        if (neParam.EMAIL_SECURE === "false") secure = false;
        else secure = true;

        let transporter = nodemailer.createTransport({
          host: neParam.EMAIL_HOST,
          port: neParam.EMAIL_PORT,
          secure: secure, // true for 465, false for other ports
          auth: {
            user: neParam.EMAIL_USER,
            pass: neParam.EMAIL_PASS,
          },
        });

        const handlebarOptions = {
          viewEngine: {
            partialsDir: path.resolve(pathNotif),
            defaultLayout: false,
          },
          viewPath: path.resolve(pathNotif),
        };

        transporter.use("compile", hbs(handlebarOptions));

        let mailOptions = {
          from: neParam.EMAIL_FROM + "<example.gmail.com>", // sender address
          to: emailRow, // list of receivers
          subject: "Email Notifikasi", // Subject line
          template: "notifikasi",
          context: {
            http: neParam.EMAIL_HTTP, //neParam.EMAIL_HTTP,
            name: emailRow,
            message: message,
          },
        };
        // send mail with defined transport object
        let info = await transporter.sendMail(mailOptions);

        callback(info);
        getResponse = {
          status: "00",
          notifCallback: info,
        };
      }
      return getResponse;
    } catch (err) {
      console.log(err);
      return err;
    }
  },
  sendlNotifikasi: async function (user, message, callback) {
    try {
      await getParam();

      console.log("MASUK SEND MAIL VERIFIKASI >>>>");

      // create reusable transporter object using the default SMTP transport
      var secure;
      if (neParam.EMAIL_SECURE === "false") secure = false;
      else secure = true;

      console.log(secure);
      let transporter = nodemailer.createTransport({
        host: neParam.EMAIL_HOST,
        port: neParam.EMAIL_PORT,
        secure: secure, // true for 465, false for other ports
        auth: {
          user: neParam.EMAIL_USER,
          pass: neParam.EMAIL_PASS,
        },
      });

      const handlebarOptions = {
        viewEngine: {
          partialsDir: path.resolve(pathNotif),
          defaultLayout: false,
        },
        viewPath: path.resolve(pathNotif),
      };

      transporter.use("compile", hbs(handlebarOptions));

      let mailOptions = {
        from: neParam.EMAIL_FROM + "<example.gmail.com>", // sender address
        to: user, // list of receivers
        subject: "Email Notifikasi", // Subject line
        template: "notifikasi",
        context: {
          http: neParam.EMAIL_HTTP, //neParam.EMAIL_HTTP,
          message: message,
        },
      };
      // send mail with defined transport object
      let info = await transporter.sendMail(mailOptions);

      callback(info);
      getResponse = {
        status: "00",
        notifCallback: info,
      };
      return getResponse;
    } catch (err) {
      console.log(err);
      return err;
    }
  },
};

module.exports = notifikasi;
