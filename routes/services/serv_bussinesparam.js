const express = require("express"),
  app = express();
const router = express.Router();

const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");

router.use(checkAuth);

router.get("/bpla", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;
    // console.log(">>>> GET TENANTS >>>> " + dcodeInfo.leveltenant);
    /*
    var query =
      "SELECT id, category, label, valuestr, valueint, created_at, updated_at FROM public.mst_bussines_param order by category, id;";
    const resp = await pool.query(query, []);
    */
    const resp = await pool
      .select(
        "id",
        "category",
        "label",
        "valuestr",
        "valueint",
        "created_at",
        "updated_at"
      )
      .from("mst_bussines_param")
      .orderBy("category", "id");

    var jsonbody = resp;
    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/bpbycategory/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;
    const category = req.params.id;
    // console.log(">>>> GET TENANTS >>>> " + dcodeInfo.leveltenant);
    /*
    var query =
      "SELECT id, category, label, valuestr, valueint, created_at, updated_at FROM public.mst_bussines_param WHERE category=$1 order by category, id;";
    const resp = await pool.query(query, [category]);
    */

    const resp = await pool("mst_bussines_param")
      .where({ category: category })
      .select("*");

    console.log(resp);
    console.log(category);
    var jsonbody = resp;
    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});
module.exports = router;
