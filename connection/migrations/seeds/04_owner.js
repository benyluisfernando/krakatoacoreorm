exports.seed = function(knex) {
    // Deletes ALL existing entries
    return knex('cdid_owner')
        .del()
        .then(function() {
            // Inserts seed entries
            return knex('cdid_owner')
            .insert([
                {
                    ownname:'PT Metrodata Tbk',
                    ownstatus:1,
                    owntype:0,
                }
            ])
        })
}