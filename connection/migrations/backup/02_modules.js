exports.seed = async function(knex) {
    // Deletes ALL existing entries
    const result = await knex('mst_application').orderBy('id', 'desc');
    if(result.length > 0){
        let idapp = result[0].id;
        console.log(">>>>>>>>>>>> insert Modules >>>>>");
        // return knex('mst_application')
        return await knex('mst_module')
        .del()
        .then(function() {
            return knex('mst_module')
            .insert([
                {
                    modulename:'Users Management',
                    created_byid:0,
                    status:1,
                    idapplication:idapp,
                    modulecode:'BIFAST001'
                },
                {
                    modulename:'Group & ACL Komi',
                    created_byid:0,
                    status:1,
                    idapplication:idapp,
                    modulecode:'BIFAST002'
                },
                {
                    modulename:'Settings',
                    created_byid:0,
                    status:1,
                    idapplication:idapp,
                    modulecode:'BIFAST003'
                },
                {
                    modulename:'Master',
                    created_byid:0,
                    status:1,
                    idapplication:idapp,
                    modulecode:'BIFAST004'
                },
                {
                    modulename:'Rekonsiliasi',
                    created_byid:0,
                    status:1,
                    idapplication:idapp,
                    modulecode:'BIFAST005'
                },
                {
                    modulename:'Monitoring',
                    created_byid:0,
                    status:1,
                    idapplication:idapp,
                    modulecode:'BIFAST006'
                },
                {
                    modulename:'Reports',
                    created_byid:0,
                    status:1,
                    idapplication:idapp,
                    modulecode:'BIFAST007'
                }
            ])
        })


    }
}