// Update with your config settings.

module.exports = {
  development: {
    client: "pg",
    connection: {
      // host: "10.11.100.116",
      // database: "kktadmdb",
      // user: "postgres",
      // password: "postgres",
      // host: "182.169.41.152",
      host: "localhost",
      database: "postgres",
      //  database: "kktportaldb",
      user: "postgres",
      // password: "P@ssw0rd.1",
      password: "P@ssw0rd.1",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "pgmigra",
    },
  },
  // development: {
  //   client: "oracledb",
  //   connection: {
  //     user: "KKTORA",
  //     password: "P@ssw0rd.1", // mypw contains the hr schema password
  //     connectString: "182.169.41.225:1521/XE",
  //     database: "KKTORA",
  //   },
  //   migrations: {
  //     tableName: "oram",
  //   },
  // },
  // development: {
  //   client: "mssql",
  //   connection: {
  //     server: "182.169.41.160",
  //     user: "s1sql",
  //     password: "Password.1",
  //     database: "kktadm",
  //   },
  // },
};
