const logger = require("../routes/services/utils/logger");
module.exports = async (err, req, res, next) => {
  try {
    const fileLog = logger.logging("Krakatoa-Error-Log");
    fileLog.error(
      `Request error on url : ${req.url}. Error message, ${err.message}`
    );
    fileLog.error(`Error stack, ${err.stack}`);
    //console.log(err);
  } catch (err) {
    console.log(err);
  }
  next();
};
